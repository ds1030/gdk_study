## IO Class
- 상속관계의 stream
  - iostream
    - fstream : 유효범위 벗어날 경우 자동 close()
    - sstream
- char vs wchar_t
  - wcin, wcout, wcerr
- IO 객체의 특징
  - 복사 , 대입 불가
  - 시스템에 의존적인 iostate 지원
    - badbit / failbit / eofbit / goodbit
  - endl , flush
  - unibuf <-> nounitbuf : 출력 버퍼를 쓰지 않고 바로 보낸다.
- fstream
  - in
  - out
  - app
  - ate
  - trunc
  - binary

```
ifstream file(filename, ifstream::in | fstream::binary | ifstream::ate);
size_t filesize = file.tellg();
char* filedata = new char[(int)filesize];
file.seekg(0, ifstream::beg);
file.read(filedata, filesize);
file.close();
```

- sstream
  - istringstream
  - ostringstream
  
