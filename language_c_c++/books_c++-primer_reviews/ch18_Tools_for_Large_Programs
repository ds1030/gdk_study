## 예외 처리
예외 처리를 사용하면 실행 중 발생한 문제 감지와 문제 해결을 분리할 수 있다.

### 스택 풀기
```
using namespace std;

void func1() { throw 0; }
void func2() { func1(); }
void func3() { func2(); }
void func4() { func3(); }

int main()
{
    try {
        func4();
    } catch (int exception) {
        cout << "예외 발생, " << exception << "!" << endl;
    }
    return 0;
}
```
- 일치하는 catch가 없으면 프로그램을 종료시킨다.
- 스택풀기과정에서 구역을 빠져나가면 지역객체도 적절히 소멸함을 보장한다.
- 스택풀기중 소멸자를 호출하게 되므로 소멸자에서는 예외를 발생하게 해서는 안됨.

```
void f()
{
  shared_ptr<int> sp(new int(42)); // allocate a new object
} // 함수를 마칠 때 자동으로 shared_ptr 해제

void f()
{
  int *ip = new int(42);
  // f 안에서 잡지 못하는 예외를 발생하는 코###
  delete ip;
}
```


### 예외 잡기
- catch 절 순서는 상위 예외 클래스가 하위 예외 클래스보다 아래 위치하도록 작성.
- 더 상위 호출에서 예외 처리를 해야한다 판단될때는 예외 다시 던지기(throw)
- 모든 예외 처리자 "catch(...)""

### 함수 try 구역과 생성자
생성자 초기값에서 발생한 예외를 처리하려면 생성자를 함수 try 구역으로 만들어야함
```
template <typename T>
Blob<T>::Blob(std::initializer_list<T> il) try :
                                                data(std::make_shared<std::vector<T>>(il)) {
 /* empty body */
} catch(const std::bad_alloc &e) { handle_out_of_memory(e); }
```

### noexcept 예외 지정
```
void recoup(int) noexcept; // won't throw
void alloc(int);           // might throw
```
예외 지정 위반을 해도 컴파일 에러는 나지 않는다.

- noexcept 연산자
```
noexcept(recoup(i))   //recoup 호출에서 예외를 발생하지 않으면 true, 그외 false.
void f() noexcept(noexcept(g()));
```

### 표준 exception 클래스 계통
p.931 참고

### 사용자 정의 예외 타입
```
#ifndef BOOKEXCEPT_H
#define BOOKEXCEPT_H
#include <stdexcept>
#include <string>

// hypothetical exception classes for a bookstore application
class out_of_stock: public std::runtime_error {
public:
    explicit out_of_stock(const std::string &s):
                       std::runtime_error(s) { }
};

class isbn_mismatch: public std::logic_error {
public:
    explicit isbn_mismatch(const std::string &s):
                          std::logic_error(s) { }
    isbn_mismatch(const std::string &s,
        const std::string &lhs, const std::string &rhs):
        std::logic_error(s), left(lhs), right(rhs) { }
    const std::string left, right;
};

#endif
```

## 네임스페이스
클래스, 함수, 템플릿과 같은 전역 이름의 충돌을 막기 위해 사용

http://demianmedich.tistory.com/19

## 다중과 가상 상속
### 다중 상속
```
class A{};
class B:public A;
class C;
class D:public B, public C;
```
- 클래스 생성자 및 복사제어멤버 호출 순서는 A->B->C->D
- 소멸자 호출 순서는 역순.

```
struct Base1 {
  Base1() = default;
  Base1(const std::string&);
  Base1(std::shared_ptr<int>);
};

struct Base2 {
  Base2() = default;
  Base2(const std::string&);
  Base2(int);
};


struct D1: public Base1, public Base2 {
  using Base1::Base1;
  using Base2::Base2;
};

struct D2: public Base1, public Base2 {
  using Base1::Base1;
  using Base2::Base2;

  D2(const string &s): Base1(s), Base2(s) { }
  D2() = default;
}
```
### 변환과 다중 기초 클래스
p.959

### 다중 상속에서 클래스 유효 범위
- 상위 클래스에 같은 이름의 함수가 있을때, 하위 클래스에선 잠재적인 모호함을 없애줘야한다.

```
double Panda::max_weight() const
{
  return std::max(ZooAnimal::max_weight(), Endangered::max_weight());
}

```
### 가상 상속
p.965

```
class Raccoon : public virtual ZooAnimal { /* ... */ };
class Bear : virtual public ZooAnimal { /* ... */ };
```
### 생성자와 가상 상속
p.968
