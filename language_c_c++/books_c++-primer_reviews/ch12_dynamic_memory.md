## 동적 메모리
동적 메모리는 heap 메모리 영역을 사용하는 것으로 c++ 에서는 new 와 delete를 통해서 생성과 소멸을 한다. 이런 방법을 쉽고 안전하게 하게 하기 위해서 나온 기능이 스마트 포인터이다. 스마트 포인터는 memory 헤더에서 정의한다.
- shared_ptr
  같은 객체를 여러 포인터에서 참조하도록
  - make_shared 함수 : 동적 메모리를 할당하고 사용하는 가장 안전한 방법
  ex) shared_ptr<int> p3 = make_shared<int>(42);  // 값이 42인 int를 가리키는 shared_ptr
- unique_ptr
  가리키는 객체를 하나의 소유자만 가질수 있다
- weak_ptr

동적 메모리를 사용 하는 목적
1. 얼마나 많은 객체가 필요한지 모른다.
2. 필요한 객체 타입을 정확히 모른다.
3. 여러 객체에서 데이터를 공요하려 한다.

(1) 다음중 r, p, q이 가리키는 값과 참조 카운터는?
```
auto r = make_shared<int>(1); 1
auto p = make_shared<int>(2); 2
auto q = make_shared<int>(3); 3
r = p;
r = q;
*p += 1;
```
sol) r, p 는 같은 변수를 가리키고 있다.
val r : 3     p : 3     q : 3
ref r : 2     p : 1     q : 2


(2) shared_ptr의 초기화 방법으로 옳지 않은 것은
```
shared_ptr<int> p1 = new int(100);
shared_ptr<int> p2(new int(100));
shared_ptr<int> p3 = make_shared<int>(100);
```
sol) 1 / 내장 포인터를 대입 할 수 없다. shared_ptr<int> p1(new int(100))


shared_ptr를 생성하는데 make_shared를 쓰는게 좋다!!! 그리고 절대로 get()을 이용해서 새로운 shared_ptr을 만들지 마라

(3) 다음에서 발생하는 문제는 무엇인가?
```
{
  shared_ptr<int> p(new int(42));
  shared_ptr<int> q(p.get());
}
```
sol) 스마트 포인터를 사용하는 이유가 없어진다. 하나의 메모리 영역을 두 쉐어드 포인터가 가리키게 되어 둘 중 하나가 해제 되면 메모리 영역이 제거되고 나머지 하나의 쉐어트 포인터는 잘못된 메모리 번지를 가리키게 된다. 나머지가 딜리트 되면 프로그램이 메모리 참조 에러가 생긴다. 가장 많이 실수하는 부분!!!


(4) 다음중 포인터 변수가 가리키는 값이 다른 하나는?
```
int *p1 = new int;
char *p3 = new char();
float *p4 = new float();
```
sol) 2,3 은 값이 0이지만 1은 쓰레기 값을 가리키고 있다. 포인터 변수의 초기화 문제, 2,3은 생성자를 이용해서 초기화를 한 경우.


(5) 다음중 auto의 사용으로 올바르지 않은 것은?
```
string obj = "hello";

auto p1 = new auto(obj);
auto p2 = new auto{a,b,c};
auto p3 = new int(1);
```
sol) 2 / auto 는 인자값을 통해서 타입을 추론하는 키워드 이다. 인자값이 여러개인 경우는 지원하지 않는다. 즉 중괄호는 안되고 무조건 소괄호를 이용해야 하고 값을 하나만 써야 한다.


(6) 다음중 메모리 해제가 정상적이지 않은 것은?
```
int i;
int *pi1 = &i;
int *pi2 = nullptr;

double *pd = new double(33);
double *pd2 = pd;

delete i;     // 1
delete pi1;   // 2
delete pd2;   // 3
delete pi2;   // 4
```
sol) 1 / 포인터가 아니다
2 / new를 이용해서 생성하지 않은 메모리 영역이다


new 와 delete를 사용해 메모리를 관리할 때 흔히 발생하는 문제점
1. 메모리를 delete 하는 것을 잊는다. 동적 메모리는 프로그램이 종료 되기 전에는 자동으로 해제되지 않는다.
2. 삭제한 객체를 사용한다.
3. 같은 메모리를 두 번 삭제한다. 이 오류는 두 포인터에서 동일한 메모리를 가리킬 때 발생한다.

오로지 스마트 포인터만 사용한다면 이러한 문제를 모두 피할 수 있다. 스마트 포인터에서는 해당 메모리를 가리키는 스마트 포인터가 없을 때에멘 그 메모리를 삭제한다.

(7) 두 상황의 차이를 설명하라
```
int *q = new int(42);
int *r = new int(10);
r = q;

auto q2 = make_shared<int>(42);
auto r2 = make_shared<int>(100);
r2 = q2;
```
sol) 위에 q로 생성한 메모리 영역은 삭제가 안되고 남아있고 q2는 구역을 나가면 메모리가 자동으로 삭제 된다.


shared_ptr p 에서 p.reset() 함수의 정확한 동작은 p가 해당 객체를 가리키는 유일한 shared 포인터일 때 p의 기존 객체를 해제 하고 p는 nullptr이 된다.
p.reset(q) 내장포인터 q를 전달할 경우엔 p는 q를 가리킨다.

(8) 포인터 변수 p1이 가리키는 값은?
```
void process(shared_ptr<int> ptr){
  *ptr = *ptr + 1;
}

int *p1 = new int(100);
process(shared_ptr<int>(p1));
cout << *p1 << endl;
```
sol) 쓰레기값 런타임 오류!!!


스마트 포인터 타입에서는 get 함수를 정의하는데, 이 함수에서는 스마트 포인터에서 관리하는 객체에 대한 내장 포인터를 반환한다.
```
[주의사항] get은 포인터를 delete 하지 않음이 확실한 코드에서 포인터에 접근할 수 있도록 할 때만 사용한다. 특히 절대 get을 사용해 스마트 포인터를 초기화하거나 대입하지 않는다.
shared_ptr<int> p(new int(42));
shared_ptr<int> q(p.get());
```

(9) 포인터 변수 p가 가리키는 값은?
```
shared_ptr<int> p(new int(42));
int *q = p.get();
{
  *q = *q + 1;
  shared_ptr<int> r(q);
  *q = *q + 1;
}
cout << *p << endl;
```
<<<<<<< HEAD
sol) 쓰레기값 런타임 오류!!!


모든 포인터를 shared 포인터로 바꾸고 shared 포인터는 make_shared를 통해 만든다!!!
=======

## Dynamic memory
- Recall) Global objects, local static objects, dynamic objects의 생성/소멸과 Stack/heap memory의 관계

- 553p Caution: Smart Pointer Pitfalls
  - 2개 이상의 smart pointer에 동일한 build-in pointer로 initialize하지 말 것
  - get()으로 return된 pointer를 delete하지 말 것
  - initialize하거나 reset하기 위해서 get function을 사용하지 말 것
  - 만일 get을 이용해서 return한 pointer를 사용하는 경우, 마지막 smart pointer가 소멸하는 경우 해당 포인터는 invalid해지는 것을 계속 기억할 것
  - new로 할당하지 않은 리소스를 관리하는 스마트포인터의 경우 deleter를 만들어서 전달해야한다. (communication 예제 기억)

- Note) Race condition 등에 대해 퍼포먼스를 고민해야 할 수 있다.

### unique_ptr
- 소유 개념, Reference count를 안함.
- Private member, 싱글턴, Factory method pattern에 많이 사용됨.

## Allocator
- Memory를 Allocation 시키는 것과 construction 시키는 것을 구분한다.
- Performance 관점에서 매우 효율적일 것 같아 추가 스터디 진행해보자.
>>>>>>> 018bb007b5f48a16a61860d933cfdd8feb786a10
