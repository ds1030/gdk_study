# 템플릿의 목적
- 일반화 프로그래밍

# 타입 독립적인 코드 만들기
- 템플릿 함수나 클래스 자체의 목적은 클래스나 함수를 타입에 독립적으로 만들기 위함.
- 즉, 타입이 바뀌어도 함수나 클래스는 변하지 않아야 한다.
- 타입에 대해 요구하는 조건 수를 최소화하도록 짜야 한다.
- 예로 들어, compare 함수를 만들 때는 비교 연산자<,> 두 개를 사용하는 것보다 > 한 개만 사용해서 짜는 것이 더 독립성과 이식성을 높이고,
- 좀 더 나아가서 less를 사용해 함수를 정의하면, std에 의존하기 때문에 안정성이 커진다.

# 템플릿 컴파일 과정
- 컴파일러는 템플릿 정의를 볼 때 코드를 생성하는 것이 아니라, 템플릿을 사용할 때만 코드를 생성한다.
- --> 소스코드를 조직하는 방법과 오류를 발견하는 시점에 영향을 준다.
- 컴파일하기 위해서는 컴파일러는 템플릿 정의가 필요하다. 따라서 헤더에 텟플릿 정의도 위치시킨다.

# 템플릿의 종류
- 대상이 타입이냐 함수냐에 따라
  - 템플릿 클래스
    - 클래스 멤버 템플릿 함수
    - 클래스 멤버 템플릿 변수
  - 템플릿 함수
    - 일반 템플리 함수
    - 인라인 템플릿 함수
    - constexpr 템플릿 함수
- 템플릿 파라미터가 타입이냐 비타입이냐에 따라
  - 비타입 템플릿 매개변수
  - 타입 템플릿 매개변수

# 템플릿 정의
- 템플릿 클래스
  - Inside
  - Outside
```
template <typename T>
void Blob<T>::check(size_type i, const std::string &msg) const
{
    if (i >= data->size())
        throw std::out_of_range(msg);
}
```
- 템플릿 함수
- 특이사항
  - 템플릿 클래스 정의 안에서는, 템플릿타입을 명시할 필요가 없다.
```
template <typename T>
BlobPtr<T> BlobPtr<T>::operator++(int)
{
    // no check needed here; the call to prefix increment will do the check
    BlobPtr ret = *this;  // save the current value
    ++*this;    // advance one element; prefix ++ checks the increment
    return ret;  // return the saved state
}
```
  - 비템플릿 타입 Friend 멤버 vs 템플릿 타입 Friend 멤버

    ```
    template <typename T> class Pal;
    class C {  //  C is an ordinary, nontemplate class
        friend class Pal<C>;  // Pal instantiated with class C is a friend to C

        // all instances of Pal2 are friends to C;
        // no forward declaration required when we befriend all instantiations
        template <typename T> friend class Pal2;
    };

    template <typename T> class C2 { // C2 is itself a class template

        // each instantiation of C2 has the same instance of Pal as a friend
        friend class Pal<T>;  // a template declaration for Pal must be in scope

        // all instances of Pal2 are friends of each instance of C2; no prior declaration needed
        template <typename X> friend class Pal2;

        // Pal3 is a nontemplate class that is a friend of every instance of C2
        friend class Pal3;    // prior declaration for Pal3 not needed
    };

    template <typename Type> class Bar {
    friend Type; // grants access to the type used to instantiate Bar
        //  ...
    };
    ```
  - C++11, 템플릿은 타입이 아니기 때문에 typedef이 불가능하지만 using은 가능하다.
  - C++11, 템플릿 타입 매개변수에 인자로 디폴트를 정의할 수도 있다.
    ```
    // compare has a default template argument, less<T>
    // and a default function argument, F()
    template <typename T, typename F = less<T>>
    int compare(const T &v1, const T &v2, F f = F())
    {
        if (f(v1, v2)) return -1;
        if (f(v2, v1)) return 1;
        return 0;
    }

    Lippman, Stanley B.. C++ Primer (pp. 670-671). Pearson Education. Kindle Edition.
  ```
  - 템플릿 오버로딩
  - 템플릿 인자 deduction: 타입을 명시하지 않으며 컴파일러가 타입을 추론해서 해당하는 템플릿 함수를 인스턴스한다. 이 때, 타입 캐스팅보다는 템플릿 함수를 찾는 것을 지향한다.

- Variadic template
```
// function to end the recursion and print the last element
// this function must be declared before the variadic version of print is defined
template<typename T>
ostream &print(ostream &os, const T &t)
{
    return os << t; // no separator after the last element in the pack
}
// this version of print will be called for all but the last element in the pack
template <typename T, typename... Args>
ostream &print(ostream &os, const T &t, const Args&... rest)
{
    os << t << ", ";           // print the first argument
    return print(os, rest...); // recursive call; print the other arguments
}
```

- Template Specialization
```
// first version; can compare any two types
template <typename T> int compare(const T&, const T&);
const char *p1 = "hi", *p2 = "mom";
compare(p1, p2);      // calls the first template
compare("hi", "mom"); // calls the template with two nontype parameters

template <>
int compare(const char* const &p1, const char* const &p2)
{
    return strcmp(p1, p2);
}
```

# 사용
- 명시적 템플릿 파라미터 타입 전달
- 암시적 템플릿 파라미터 타입 전달
