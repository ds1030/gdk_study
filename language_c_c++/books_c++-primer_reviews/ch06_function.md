## Function
이름이 있는 {}

## Call Operator ()
함수를 실행하기 위한 연산자로 피연산자 타입으로 (함수나 함수포인터에 대한 표현식)을 받는다.

## 함수호출
- 함수가 호출되면, 가장 먼저 파라미터가 아규먼트로 초기화된다.
- 예로, int fact(int val){}이라는 함수를 fact(5)로 호출하면, 함수 안에서 가장 먼저 일어나는 일은
- var이라는 이름의 정수형 타입 변수를 로컬 변수로 5라는 값으로 정의하는 것. int val = 5;

## 함수 반환타입
- 배열타입이나 함수타입은 반환타입으로 쓸 수 없다.
- 대신 배열이나 함수에 대한 포인터를 반환할 수 있다.

## 자동객체 (automatic object)
- 구역{}을 실행하는 동안에만 존재하는 객체

## 지역 static 객체
- 프로그램 실행부터 끝날 때까지 이름구역{} 안에서만 찾을 수 있는 객체

## 함수 선언
- 변수와 마찬가지로 정의는 한 번만, 선언은 여러 번.
- 단, 함수를 사용하지 않는 그 함수를 정의하지 않아도 선언할 수 있다.
- 선언: 함수이름과 반환타입까지가 선언의 범위. 함수 매개변수는 구현에 해당되기 때문에 꼭 선언에 포함할 필요는 없다.
- 함수 선언 = 함수 원형(function prototype)
```
int fact();
```
- 정의
```
int fact() {
  // Do something...
}
```
- 참조자를 사용해 복사 피하기
```
bool isShorter(const string &s1, const string &s2)
{
  return s1.size() < s2.size();
}
```

## const 매개변수와 인자
- const int ci = 42;
- int i = ci
- int * const p = &i;
- (* p)=0;

## 인자 전달
- 매개변수 초기화는 변수 초기화와 같은 방식으로 작동한다.
- call by reference vs call by value vs call by pointer
- pointer parameters
```
void reset(int *ip) {
  *ip = 0;
  ip = 0;
}
```
- 참조자를 사용해 복사 피하기

## 가능하면 const에 대한 참조자를 사용한다.
- 호출자에게 값을 변경하지 않는다는 보장을 해준다.
- 함수에 사용할 수 있는 인자타입을 폭넓게 한다.
- find_char(string &s) vs find_char(const string &s)

## 배열 매개변수
- 배열은 복사할 수 없으며, 배열을 사용할 때는 해당 배열의 첫 요소에 대한 포인터로 전달
- 다만!! 배열을 값으로 전달할 수 없을지라도 매개변수를 배열처럼 보이도록 만들 수 있다.
```
void print(const int*)
void print(const int[])
void print(const int[10])
```
- 배열 파라미터는 포인터로 전달되기 때문에 그 크기를 알 방법이 없어서 호출자가 추가정보를 제공해야 한다. 그 방법은 총 3가지가 있다.
  - 표시자를 사용한 배열 범위 지정하기: 문자열의 null pointer 표시자
  - 표준 라이브러리 변환 사용하기: begin과 end를 사용해 배열의 첫 포인터와 마지막 요소 바로 다음 포인터를 전달
  - 크기 매개변수를 명시적으로 전달하기

## 배열 참조자 매개변수
```
void print(int (&arr) [10]) {

}
```

## 매개변수가 가변인 함수
- 모든 인자 타입이 같은 경우 initializer_list 라이브러리 타입
  - initializer_list vs std::vector [참고](https://stackoverflow.com/questions/14414832/why-use-initializer-list-instead-of-vector-in-parameters)
- 인자타입이 다양하면 가변인자 템플릿 variadic tamplate
- 매개변수 생략 ...  ellipsis

## 값을 반환하는 함수
- 반환 값은 함수 반환타입과 타입이 같거나 그 타입으로 암시적 변환을 할 수 있어야 한다.

## 값을 반환하는 법
- 값은 변수와 매개변수를 초기화하는 것과 정확히 같은 방식으로 반환한다.
- 즉 반환 값은 호출하는 쪽의 임시 객체를 초기화하는 데 사용하며 그 임시객체가 함수호출결과.

## 참조자 반환은 좌변 값이다.

## 배열에 대한 포인터 반환하기
```
typedef int arrT[10];
using arrT = int[10];

arrT* func(int i);
```

## 후행 반환타입
```
auto func(int i) -> int(*)[10];
```

## 기본인자
```
string screen(int h = 24, int w = 80, char background '');
```

## inline
- 함수호출은 동일한 표현식을 평가하는 것보다 더 느린 경향이 있다.
- 이럴 때, inline을 사용하면, 컴파일러가 판단해서 함수호출 대신 인라인으로 대체한다.
- 단, inline은 길이가 길고 프로그램 전체적으로 사용되는 함수에 사용할 경우, 프로그램의 크기가 커질 수 있다.

## constexpr 함수

## 함수에 대한 포인터
```
bool lengthCompare(const string &a, const string &b)
이 함수에 대한 포인터 변수를 선언하면?
pf = ?

함수 포인터 사용하기
pf = lengthCompare;
pf = &lengthCompare;
```

## 함수 포인터 매개변수


## 주의사항
- 함수 인자에 대한 평가 순서는 보장되지 않는다. 예로, int x=1; add(x++, x++); 에서 실제 컴파일러가 해석하는 것이 add(2,3)일지 add(3,2)일지 모른다는 것
- 함수 인자에 정의된 함수의 파라미터 타입과 다른 인자를 전달하면, 암묵적 형변환을 시도한다.
- 오버로딩
```
void fcn(const int i)
void fcn(int i)
```
```
int fcn(int i)
void fcn(int i)
```
- 매개변수 생략은 C와 C++ 모두에서 공통인 타입에 대해서만 사용해야 한다. 특히 대부분의 클래스 타입 객체는 매개변수 생략에 전달할 때 제대로 복사되지 않는다.
- 지역객체에 대한 참조자나 포인터는 절대 반환하지 않는다.
- inline과 constexpr 함수는 모두 헤더에 정의한다. 다른 함수와 달리 inline, constexpr 함수는 프로그램에서 여러 번 정의할 수 있다.
