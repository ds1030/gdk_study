## Lvalue, Rvalue
- 모든 표현식은 Lvalue이거나 Rvalue이다.
- Lvalue: 객체를 참조하는 표현식. 메모리 위치를 가지고 있음.
- Rvalue: Lvalue가 아닌 모든 것. 구분 가능한 메모리 영역을 가지는 객체로 나타낼 필요가 없는 표현식.

## Rvalue examples
- Numeric literals: 3, 3.14, ...
- enumeration 상수 구분자
```
enum OUTPUT_CONFIG{MONO, STEREO, 5.1};
OUTPUT_CONFIG myConfig = STEREO; // Fine
STEREO = MONO; // ERROR
```
- & 연산
- 산술연산 + 연산자 결과
``` a + 1 = 3 // ERROR ```
- 후위증가 연산자의 표현식의 결과
```
int a = 0;
a++; // Rvalue
a++ = 5; // ERROR - 증가되기 전 객체의 값을 임시로 복사하고, 원 객체의 값을 변경한 후 임시 복사한 객체를 리턴함. -> Rvalue, const 속성인 값을 return함.
```

## Lvalue examples
- 전위증가 연산자의 표현식의 결과
```
int a = 0;
++a; // Lvalue
++a = 5; // Lvalue
```

## 산술연산자
- 작은 정수 타입 피연산자는 큰 정수타입으로 승격됨.
- 반올림?
``` int a = static_cast<int>(b+0.5); ```
- bool 값은 계산에 사용하지 말라.

## 전위증가 연산자와 후위증가 연산자의 Pseudo-code
```
class Integer
{
  public:
  Integer& operator++() // pre-increment operator
  {
    x_ += 1;
    return *this;
  }
  const Integer operator++(int) // post-increment operator
  {
    Integer temp = *this;
    ++*this;
    return temp;
  }
  private:
  int x_;
};
```
