## 클래스
클래스에서는 추상 데이터 타입을 정의해서 사용하며 추상 데이터 타입을 사용하면 클래스 구현을 어떻게 할지는 클래스 설계자가 고민할 뿐, 클래스를 사용하는 프로그래머는 해당 타입이 어떻게 작동하는지 알 필요가 없다. 대신 그 타입이 하는 일에 대해 추상적으로 생각할 수 있을 뿐이다.
### 데이터 추상화
인터페이스와 구현의 분리에 의존하는 프로그래밍 기법.
### 캡슐화


### const 멤버 함수
gdk_base 에서도 쓰고 있는 함수이다. 간단하게 클래스 멤버 변수를 읽기 전용으로 만든다는 의미이다.


### 클래스 생성자
Sales_data() = default; -> 컴파일러에서 그 생성자를 만들어 내도록 요청

Sales_data(const std::string &s): bookNo(s){} -> 생성자 초기 값 목록 constructor initializer list

클래스 밖에서 생성자를 정의 하려면 다음과 같이 클래스 이름을 명시해야 한다.
Sales_data::Sales_data(const std::string &s){

}


### 복사, 대입, 소멸

struct 와 class 의 유일한 차이는 기본 접근 수준이다. struct 키워드는 디폴트가 public 이고 class 키워드는 디폴트가 private 이다.


### 프랜드
클래스에서는 다름 클래스나 함수를 프렌드로 지정해 자신의 public 이 아닌 멤보를 그 클래스나 함수에서 접근할 수 있도록 한다. 프렌드 선언은 클래스 정의 안에서만 할 수 있으며 클래스 안이라면 어디든 상관없다.

### 캠슐화의 이점
- 사용자 코드에서는 캡슐화한 객체 상태를 무심코 망가뜨릴 수 없다.
- 시간이 흘러도 사용자 수준 코드를 변경하지 않고 캡슐화한 클래스의 구현 내용을 변경할 수 있다.
데이터 멤버를 private 으로 정의하면 클래스 제작자는 그 데이터를 자유롭게 변경할 수 잇ㄷ. 구현 내용을 변경할 때 그 변경으로 생길 영향이 무엇인지는 해당 클래스 코드만 보면 된다. 사용자 코드는 인터페이스를 변경할 때만 바꾸면 된다. 데이터가 public이면 이전의 데이터 멤버를 사용한 모든 코드가 망가질 수 있다. 프로그램을 다시 사용할 수 있도록 이전 방식에 의존하던 모든 코드를 찾아 다시 만들어야 할 수도 있다.

### mutable 멤버 변수
mutable 키워드로 선언한 변수는 const 함수에서도 값을 변경할 수 있다.


### 전방선언
forward declaration 이라고 부르며 class Screen; 이렇게 사용한다. 선언 이후 정의가 나타나기 전까지 screen 타입은 불완전 타입(incomplete type) 이다. 즉 Screen이 클래스 타입임은 알고 있지만 그 타입에서 담고 있는 멤버가 무엇인지는 모른다. 불완전 타입은 제한적으로만 사용 가능한데 포인터나 참조자 정의, 매개변수나 반환 타입으로 사용하는 함수 선언. 함수 정의는 하지 못한다.

### 클래스 사이의 프렌트 관계
```
class Screen{
  friend class Window_mgr;
}
```
프렌드 클래스의 맴버 함수에서는 public이 아닌 멤버를 포함해 프렌드 관계를 허가한 클래스의 모든 멤버에 접근할 수 있다.

### 함수를 프렌드로 만들기
```
class Screen{
  friend void Window_mgr::clear(ScreenIndex);
}
```

### 프렌드 선언과 유효 범위
프렌트 선언은 일반적인 함수 선언이 아니므로 반드시 함수를 선언해 줘야 한다.

### 클래스 유효 범위와 클래스 밖에서 정의한 멤버
```
class Window_mgr{
  public:
    ScreenIndex addScreen(const Screen&);
};

Window_mgr::ScreenIndex Window_mgr::addScreen(const Screen &s){
  screens.push_back(s);
  return screens.size() - 1;
}
```
파라미터와 함수 안은 클래스 유효 범위지만 함수 네임과 반환형은 클래스 밖이다.

### 이름 검색과 클래스 유효 범위

```
typedef double Money;
string bal;

class Account{
  public:
    Money balance(){ return bal; }
  private:
    Money bal;
};
```
검색 순서
- 먼저 이름을 사용한 구역 안에서 해당 이름의 선언을 찾는다.
- 이름을 찾지 못하면 이를 둘러싸고 있는 유효 범위에서 찾는다.
- 선언을 찾지 못하면 프로그램에서 오류를 발생한다.

클래스 정의 단계
- 먼저 멤버 선언을 컴파일한다.
- 클래스 전체가 나타난 후에야 함수 본체를 컴파일한다.

### 클래스 생성자
const 멤버 변수의 초기화를 위해선 반드시 생서자 초기 값 목록을 사용해야 한다.
```
constRef::ConstRef(int ii){
  i = ii;
}

constRef::ConstRef(int ii): i(ii){}
```
생성자 초기 값 목록에서는 멤버를 초기화하는 데 사용하는 값만 지정할 뿐 초기화를 수행하는 순서는 지정하지 않는다. 순서는 클래스 정의에 나타나는 순서로 초기화한다.

### 위임 생성자 delegating constructor
```
class Sales_data{
  public:
    Sales_data(std::string s, unsigned cnt, double price): bookNo(s), units_sold(cnt), revenue(cnt * price){}
    Sales_data(): Sales_data("", 0, 0){}
    Sales_data(std:: string s): Sales_data(s, 0, 0){}
    Sales_data(std::istream &is): Sales_data(){
      read(is, *this);
    }
}
```
하나의 생성자에서 초기화를 하고 나머지 생성자에서는 초기화를 위임한다.

### 생성자에서 정의한 함시적 변환 막기
explicit 키워드는 인자 하나로 호출할 수 있는 생성자에만 의미가 있다. 여러 인자가 필요한 생성자는 함시적 변환을 수행하는 데 사용할 수 없으므로 이러한 생성자는 explicit로 지정할 필요가 없다.
explicit 키워드는 클래스 헤더에 있는 생성자 선언에서만 허용한다.
static_cast 를 사용하명 명시적 형변환이 가능하다.

### 집합클래스
struct 특징
- 모든 데이터 멤버가 public이다.
- 생성자를 전혀 정의하지 않는다
- 클래스 내 초기 값이 없다
- 기초 클래스나 virtual 함수가 없다

단점
- 클래스의 모든 데이터 멤버는 public이다
- 모든 객체의 멤버를 올바르게 초기화하기 위해 클래스 사용자에게 짐을 지운다
- 멤버를 추가하거나 삭제할 때 모든 초기화를 갱신해야 한다

### OOP 5대 원칙
http://www.nextree.co.kr/p6960/
