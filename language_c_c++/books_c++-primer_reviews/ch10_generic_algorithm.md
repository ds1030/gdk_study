# Summary
## 10.1 개관
 서로 다른 타입에 대한 정렬, 검색과 같은 알고리즘을 일반화 하여 정의
 algorithm, numeric header
 일반적으로 알고리즘은 컨테이너에 직접 연산 X : 두 반복자 범위를 훑으며 연산

```
int ia[] = {27 , 210, 12, 47, 109, 83}
int val = 83;
int *result = find(begin(ia), end(ia), val);

auto result = find(ia + 1, ia + 4, val)
```
반복자 때문에 알고리즘 컨테이너는 독립적일 수 있다.


## 10.2 알고리즘 처음 살펴보기
### 10.2.1 읽기 전용 알고리즘
입력 범위 내 요소를 읽기만 하고 쓰지는 않는 알고리즘
find, count, accumulate, equal etc...
```
int sum = accumulate(vec.cbegin(), vec.cend(), 0);
string sum = accumulate(vec.cbegin(), vec.cend(), string(""));
equal(a.cbegin(), a.cend(), b.cbegin());
```
세번째 인자의 타입과 반환값의 타입이 일치해야함
두번째 순차열이 최소한 첫번째 순차열보다 같거나 크다고 가정

### 10.2.2 컨테이너 요소에 기록하는 알고리즘
컨테이너 연산을 수행하지 않으므로 크기 자체 변경 x
```
fill(vec.begin(), vec.end(), 0);
fill_n(vec.begin(), vec.size(), 0);
```
gdk에서 vector 초기화에 써먹어 볼수 있지 않을까?

1. back_inserter
알고리즘에 출력 내용을 담을 만큼 충분한 요소가 있을 보장하는 것
```
fill_n(back_inserter(vec), 10, 0);
```
vec.pushback(0)을 10번 수행 하는 것과 같다.

2. 복사 알고리즘
```
auto ret = copy(begin(a1), end(a1), a2);
replace(list.begin(), list.end(), 0 , 42);
```

### 10.2.3 컨테이너 요소에 재정렬하는 알고리즘
sort, unique

```
sort(words.begin(), words.end());
auto end_unique = unique(words.begin(), word.end());
words.erase(end_unique, words.end());
```

## 10.3 연산을 사용자 정의 하기
### 10.3.1 알고리즘에 함수 전달하기
1. 술어함수(predicate)
호출할 수 있는 표현식으로 조건으로 사용 할 수 있는 값
```
sort(words.begin(), words.end(), isShorter);
```

2. 정렬 알고리즘
stable_sort()
동등 조건에서 순서 유지
```
stable_sort(words.begin(), words.end(), isShorter);
```

### 10.3.2 람다 표현식
함수 호출성 객체 : 함수, 함수 포인터, 클래스, 람다
알고리즘에서 술어함수를 어떤 인자를 받는 조건으로 처리할 때
람다 표현식 형식

 [갈무리 목록] (매개변수 목록)->반환 타입{함수 본체}

일반 함수와 달리 후행 반환 타입 사용해야 한다.갈무리는 람다 표현식에서 일반 함수의 지역 변수와 같은 개념이

find_if : 특정 크기의 요소를 찾을 때 사용
```
isShorter 술어함수를 람다로 사용하는 경우 :
stable_sort(words.begin(), words.end(), [](const string &a, const string &b) {return a.size() < b.size()});
```

### 10.3.3 람다에서 갈무리와 반환
람다를 정의 하면 컴파일러에서는 그 람다에 해당하는 (이름없는) 새 클래스 타입을 생성
