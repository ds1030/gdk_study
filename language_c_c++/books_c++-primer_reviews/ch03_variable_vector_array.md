# 핵심
## 3.1 네임스페이스
* :: 범위지정 연산자
* 헤더에는 using 선언을 포함하지 않는다. 왜??????

## 3.2 string 라이브러리 타입
### 3.2.1 정의와 초기화하기
```
string s1;
string s2 = s1;
string s3 = "hiya";
string s4(10, 'c');
```
"hiya"의 타입과 사이즈는?

#### 직접 초기화와 복사 초기화
왜 중요한가?
아래는 직접초기화인가 복사 초기화인가?
```
string s1;
string s2 = s1;
string s3 = "hiya";
string s4(10, 'c');
```

### 3.2.2 string 연산
#### 상수와 string 덧셈
아래 문장에서 문제는?
```
string s4 = s1 + ", ";
string s5 = "hello" + ", ";
string s6 = s1 + ", " + "world";
string s7 = "hello" + ", " + s2;
```

## 3.3 vector 라이브러리 타입
* 객체는 타입이 모두 같은 객체의 모음이다.
* 아래에서 안되는 것은?
```
std::vector<string> a;
std::vector<string*> b;
std::vector<string&> c;
```

### 3.3.1 vector의 정의와 초기화
* 벡터를 "joey", "wilson", "tom" 목록초기화하세요.

### 3.3.2 vector에 요소 추가하기
```
vector<int> ivec;
for (decltype(ivec.size()) ix = 0; ix != 10; ++ix)
  ivec[ix] = ix;
```

## 3.4 반복자
* 컨테이너가 비었을 때 begin()에서 반환하는 반복자는 end와 같다.
* 일반적으로 begin, end()에서 반환하는 타입을 모르고 auto를 사용해 받는다.
```
string s("some string");
if (s.begin() != s.end()) {
  auto it = s.begin();
  *it = toupper(*it);
}
```

### 3.4.1 반복자 사용하기
#### 반복자 타입 [링크](https://stackoverflow.com/questions/14373934/iterator-loop-vs-index-loop)
```
vector<int>::iterator it;
string::iterator it2;
vector<int>::const_iterator it3;
string::const_iterator it4;
```

```
for (auto it = v.begin(); it != v.end(); ++it) {
    // if the current index is needed:
    auto i = std::distance(v.begin(), it);

    // access element as *it

    // any code including continue, break, return
}
```

#### begin과 end 연산
```
vector<int> v;
const vector<int> cv;
auto it1 = v.begin();
auto it2 = cv.begin();
```

## 3.5 배열
* 컴파일 시점에 차원을 알아야 하므로 차원은 반드시 상수표현식
```
int a[] = {0, 1, 2};
int a2[] = a;
```

```
int *ptrs[10];
int &refs[10];
int (*Parray) [10] = &arr;
int (&arrRef) [10] = arr;
```

```
int ia[] = {0, 2, 4, 6, 9};
int last = *(ia + 4);
```
