# Associative Container
## 핵심
- map과 set의 개념
- ordered와 unordered의 차이는?
- multi key와 unique key의 차이는?
- 그래서 다들 언제 쓸 것인가?

## 컨테이너
- map
- set

## 유일키, 다중키
- map, set, unordered_map, unordered_set
- multimap, multiset, unordered_multimap, unordered_multiset

## 순차 요소, 비순차 요소
- map, set, multimap, multiset
- unordered_map, unordered_set, unordered_multimap, unordered_multiset

## 기본
### 정의
- map<string, int> wordCounter = {{"The", 1}, {"But", 2}, {"And", 0}, {"the", 3}, {"but", 3}, {"and", 1}};
- set<string> word = {"Apple", "Banana"};

### 읽기
- 첨자 vs find()
```
map<string, int> wordCounter = {{"The", 1}, {"But", 2}, {"And", 0}, {"the", 3}, {"but", 3}, {"and", 1}};
int a = wordCounter.find("The");
int b = wordCounter["The"];
```

### 업데이트
- insert
- emplace

### 삭제
- erase

## 기타
- iterator
```
vector<int> ivec;
for(vecotor<int>::size_type i = 0; i !=10; ++i) {
  ivec.push_back(i);
  ivec.push_back(i);
}
set<int> iset(ivec.cbegin(), ivec.cend());
multiset<int> miset(ivec.cbegin(), ivec.cend());
cout << iset.size() << endl;
cout << miset.size() << endl;
```

## 주의할 점
- 일반화 알고리즘 대신 컨테이너에서 정의한 것을 사용하는 것이 낫다.
- for example, find()
