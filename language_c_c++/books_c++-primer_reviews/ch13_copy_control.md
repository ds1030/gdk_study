# 복사 제어
'복사 생성자'. '복사 대입 연산자', '이동 생성자', '이동 대입 연산자', '소멸자'
C++ 클래스는 내부를 작성하지 않아도
기본적으로 기본생성자, 복사생성자, 대입연산자, 소멸자 네 가지 멤버함수를 생성함.

## 복사 생성자
첫 매개변수가 해당 클래스 타입에 대한 참조자(&Class) 이고 추가 매개변수에 기본 값이 있는 생성자는 복사 생성자이다.
### 디폴트 복사 생성자
```
class Point
{
  int x, y;
  public:
  Point(int _x, int _y){
    x=_x;
    y=_y;
  }
  void ShowData(){
    cout<<x<<' '<<y<<endl;
  }
};
int main(void)
{
  Point p1(10, 20);
  Point p2(p1); // 디폴트 복사 생성자 호출
  p1.ShowData();
  p2.ShowData();
  return 0;
}
```
point 클래스에서 컴파일러가 만드는 디폴트 복사 생성자
```
Point(const Point& p){
  x=p.x;
  y=p.y;
}
```

디폴트 복사 생성자는 얇은 복사만 이뤄진다.
```
class AAA
{
  char* ch;
public:
  AAA(char* ch)
  {
    this->ch = new char[strlen(ch)+1];
    strcpy( this->ch, ch );
  }
  ~AAA()
  {
    delete [] ch;
  }
  void Showstr(){
    std::cout << ch << std::endl;
  }
};

void main()
{
  AAA a("test");
  a.Showstr();

  AAA b(a);
  b.Showstr();
}
```
## 복사 대입 연산자
```
class Test {
private:
    int m_i;
public:
    // 기본 생성자(default constructor)
    Test(){
        this->m_i = 0;
        cout << "call the default constructor" << endl;
    }
    // 복사 생성자(copy constructor)
    Test(const Test &t) {
        this->m_i = t.m_i;
        cout << "call the copy constructor" << endl;
    }
    // 복사 대입 연산자(copy assignment operator)
    Test& operator=(const Test &t) {
        this->m_i = t.m_i;
        cout << "call the copy assignment operator" << endl;
        return *this;
    }
};

int main() {
    Test t1;
    Test t2(t1);
    Test t3 = t1;
    t1 = t2;

    return 0;
}
```
## 소멸자
- 변수는 유효 범위를 벗어날 때 호출된다.
- 멤버가 속한 객체가 소멸할때 해당 객체의 멤버는 소멸한다.
- 컨테이너 라이브러리 또는 배열에 관계없이 컨테이너 요소는 해당 컨테이너가 소멸할때 소멸한다.
- 동적으로 할당한 객체는 그 객체에 대한 포인터에 delete 연산을 적용할때 소멸한다.
- 임시 객체는 그 임시 객체를 생성한 표현식 전체를 마칠때 소멸한다.

## default and delete keyword
복사 제어 멤버를 default,delete를 사용하여 명시하거나 금지 할 수 있다.
http://egloos.zum.com/sweeper/v/2995404
