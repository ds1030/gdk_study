##OOP(Object Oriented Programming)

### virtual
- c++에서 기초 클래스는 파생 클래스에서 재정의하길 기대하는 함수와 변경 없이 파생 클래스에서 상속받길 기대하는 함수를 반드시 구별해야 하며, 파생 클래스에서 재정의하길 기대하는 함수를 virtual로 정의한다.
- virtual 함수는 함수를 호출하는 포인터에 따라 동적으로 선택 된다.
- 생성자를 제외하고 static이 아닌 모든 멤버 함수는 virtual로 지정 가능하다.

### 기초 클래스 인터페이스 존중하기
파생 생성자의 생성자 본체에서는 public이나 protected인 기초 클래스 멤버에 값을 대입할 수 있다. 하지만 기초 클래스를 사용하는 다른 사용자와 마찬가지로 파생 클래스에서도 생성자를 사용해 상속받은 멤버를 초기화 해야 한다.

<나빠요>
```
Bulk_quote(const std::string& book, double p, std::size_t qty,
   double disc): min_qty(qty), discount(disc){
  bookNo = book;
  price = p;
}
```
<좋아요>
```
Bulk_quote(const std::string& book, double p, std::size_t qty,
   double disc): Quote(book, p), min_qty(qty), discount(disc){

}
```

### 상속 금지하기!!! > final 키워드

class NoDerived final {...};

```
class Base{...};
class Lsat final : Base{...};
class Bad : NoDerived{...};   // 오류!
```

 ### 접근 제어자
 ```
 class Base{
   protected:
    int prot_mem;  
 };

 class Sneaky : public Base{
   friend void clobber(Sneaky&);
   friend void clobber(Base&);
   int j;
 }

 void clobber(Base &b){ b.prot_mem = 0;}          // 1
 void clobber(Sneaky &s){s.j = s.prot_mem = 0;}   // 2
 ```

### 이름 검색과 상속
p->mem() or obj.mem() 이 주어질 때 다음 네 단계가 일어난다.
- 먼저 p or obj의 정적 타입을 확인한다. 멤버를 호출하므로 그 타입은 반드시 클래스 타입이어야 한다.
- p or obj 의 정적 타입에 해당하는 클래스에서 mem을 찾는다. 없다면 직접 기초 클래스에서 찾는다. 없다면 컴파일 에러!
- mem을 찾으면 해당 호출이 적법한지 확인하기 위해 그 정의에 대해 일반 타입 검사를 한다.
- 호출이 적접하다면 컴파일러에서는 코드를 생성하는데 이 코드는 해당 호출이 가상인지 여부에 따라 다르다.
- 가상인경우 : 호출이 참조자나 포인터를 통한 것이면 컴파일러에서는 실행할 버전을 해당 타입으로 동적 타입에 기초해 실행 시점에 결정한다.
- 가상이 아닌경우 : 일반 함수 호출을 생성한다.

### 상속에 의해 함수 가리기
```
struct Base {
  int memfcn();
};

struct Derived : Base {
  int emefcn(int);
};

Derived d; Base b;
b.memfcn();
d.memfcn(10);
d.memfcn();
d.Base::memfcn();
```
