# Study Quick Review
18.05.25

## Chapter 1
- standard::exit()
- C++ 파일 확장자
    - cpp: GNU
    - cc: 모든 표준을 커버하기 때문에 더 바람직함
    - .hpp .hh
- stdio -> legacy
- std::vector
- cstring v.s. string
	- cstring 쓰는게 더 좋다
- std::cin & std::cout 
	- Async로 설정 가능
- 초기화 개념
	- Direct initializer
	- ...

## Chapter 2
- 상위 const / 하위 const 개념이 제일 중요함.


## Chatper 3
- iterator
	- begin / end keyword 중요
- 다차원 배열 - 맨 위처럼 초기화하자
- for loop에서 size_t로 설정하도록 만드는게 좋겠다.

## Chapter 4
- 연산자 우선순위
	- 161p 권고:복합 표현식 다루기
	- 165p s.empty() ~ 적극 활용 -> 최적화 포인트 (왼쪽을 먼저 봄)
	- 169p 상위 레벨로 캐스팅함.
	- sizeof 연산
	- static_cast만 써야한다. static_cast를 사용하지 못하면 코드를 바꾸는게 낫다.
	- (Joey) New로 생성된 객체는 Dynamic casting밖에 안되는가?

## Chapter 5
- swicth case문 보통 언제쓰는지
- try catch
    - Catch를 꼭 안하더라도 컴파일 에러가 나지 않음.
    - IO에 관련된 부분은 꼭 쓰는 것이 바람직함.
        - GAO Handling

## Chapter 6
- 함수 프로토타입에서 파라미터 이름을 적지 않아도 된다.
    - int fact(int , int )
- const 매개변수
    - 가능하면 const에 대한 참조자를 사용하자.
- 271p 권고: 함수 이름을 다중 정의하지 말아야할때
- 289p 함수포인터
    - using keyword는 헤더에서 사용하지 않는 것이 바람직함. (함수에 대한 포인터 반환하기, 291p)

## Chapter 7
- 298p 세번째 문단
    - 추상 데이터 타입
    - 클래스 설계자는 타입이 어떻게 작동하는지 알 필요 없다.
- 299p 핵심 개념: 다른 프로그래밍 역할
- 클래스 안에서 정의한 함수는 inline함수
- 311p 생성자 초기 값 목록 사용 권장 (const 초기화 불가능)
    - 클래스 내 초기값 재지정 (bitBuffer 관련된 부분)
- 접근제어자
    - Struct와 Class는 같은 키워드이다.
        - 접근 수준만 다름. (Default가 public / private)
    - 프렌드는 안쓰는게 좋은 것 같다.
    - 317p 캡슐화의 이점
- const 함수, mutable -> 잘 안씀, getter만 const함수로 만들자.
- 전방선언, 불완전 타입 에러
- 같이 볼거리: [OOP 5대 원칙](http://www.nextree.co.kr/p6960/)

