# Add project name for unit test
project ( brian )

# create executable of unit test
add_executable ( ${PROJECT_NAME}
  ch04.cpp
  ch05.cpp
  #ch07.cpp
  # add
)

target_include_directories ( ${PROJECT_NAME} PUBLIC
)

target_link_libraries ( ${PROJECT_NAME} PUBLIC
  GTest::GTest
  GTest::Main
)

target_compile_options ( ${PROJECT_NAME} PUBLIC
  -frtti
  -fexceptions
  -Wall
  -ftree-vectorize
  -O3
  -funroll-loops
  -ffast-math
  -std=c++11
)

add_test (
    NAME ${PROJECT_NAME}_unittest
    COMMAND ${PROJECT_NAME}
    WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
)
