#include "gtest/gtest.h"

#include <stdio.h>

using namespace testing;


class Sales_data{
public:
  Sales_data(const int bookNo_, int price_) : bookNo(bookNo_), price(price_) {
  }
public:
  const int bookNo;
private:
  int price;
  friend class Dummy;
};

class Dummy{
public:
  int increasPrice(Sales_data data, int input) {
    return data.price + input;
  }
};

TEST(CH_07, Constructor) {
  const int bookNo = 1;
  int price = 100;

  // Sales_data 클래스의 멤버 변수를 초기화 하는 생성자를 만든다.
  Sales_data data(bookNo, price);

  ASSERT_EQ(data.bookNo, bookNo);
  // ASSERT_EQ(data.price, price);
}


TEST(CH_07, Friend) {
  const int bookNo = 1;
  int price = 100;
  const int input = 200;
  const int output = 300;

  Sales_data data(bookNo, price);
  Dummy dummy;

  // Sales_data 클래스의 price에 입력값을 더하고 더한 값을 반환하는 함수를 Dummy 클래스의 멤버 변수로 구현하라.
  // 단, Sales_data 클래스는 변경하지 않는다.

  ASSERT_EQ(dummy.increasPrice(data, input), output);
}
