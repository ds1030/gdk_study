#include "gtest/gtest.h"

#include <stdio.h>

using namespace testing;

int factorial(int input) {
  if(input == 1) return 1;
  return factorial(input-1) * input;
}

TEST(Function, Factorial) {
  const int input = 5;
  const int output = 1 * 2 * 3 * 4 * 5;

  // factorial 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = factorial(input);

  ASSERT_EQ(myAnswer, output);
}

int absolute(int input) {
  if(input < 0) return -input;
  return input;
}

TEST(Function, Absolute_Negative_Input) {
  const int input = -5;
  const int output = 5;

  // absolute 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = absolute(input);

  ASSERT_EQ(myAnswer, output);
}

TEST(Function, Absolute_Zero_Input) {
  const int input = 0;
  const int output = 0;

  // absolute 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = absolute(input);

  ASSERT_EQ(myAnswer, output);
}

TEST(Function, Absolute_Positive_Input) {
  const int input = 120;
  const int output = 120;

  // absolute 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = absolute(input);

  ASSERT_EQ(myAnswer, output);
}

int countCalls() {
  static int i=0;
  return i++;
}

TEST(Function, CountCalls) {
  const int ouput_when_first_call = 1;
  const int ouput_when_second_call = 2;
  const int ouput_when_third_call = 3;

  // countCalls 함수를 구현하라.

  EXPECT_EQ(countCalls(), ouput_when_first_call);
  EXPECT_EQ(countCalls(), ouput_when_second_call);
  EXPECT_EQ(countCalls(), ouput_when_third_call);
}

void Switch(int &a, int &b) {
  a = a+b;
  b = a - b;
  a = a - b;
}

TEST(Function, Switch) {
  int a = 25;
  int b = 100;

  // 두 변수의 값을 바꾸는 switch 함수를 구현하라.
  Switch(a, b);

  EXPECT_EQ(a, 100);
  EXPECT_EQ(b, 25);
}

std::string toLowercase(std::string &input) {
  for(auto& c : input) {
    c = std::tolower(c);
  }
  return input;
}

TEST(Function, ToLowercase) {
  std::string input = "ApPle";

  // 문자열 내용을 모두 소문자로 바꾸는 함수를 만든다.
  std::string myAnswer;
  myAnswer = toLowercase(input);

  EXPECT_EQ(myAnswer, "apple");
}
