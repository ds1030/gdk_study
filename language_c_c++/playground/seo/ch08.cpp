#include "gtest/gtest.h"

#include <stdio.h>
#include <vector>

using namespace testing;


TEST(CH08_SEO, Basic_01) {

  //Q. iostream 은 fstream을 상속 받았다.
  //   그래서 ofstream, ifstream 사용하는 곳에서 fstream을 사용할 수 있다.

  //   이에 대해 생각해보고 이유를 알았다면 ASSERT_EQ를 TRUE 하여라

  ASSERT_TRUE(true);
}

TEST(CH08_SEO, Basic_02) {

  //Q. file mode는
  //   in, out, app, ate, trunc, binary속성이 있다.

  //   이에 대해 생각해보고 이유를 알았다면 ASSERT_EQ를 TRUE 하여라

  ASSERT_TRUE(true);
}

TEST(CH08_SEO, Basic_03) {

 //Q. string stream은 메모리 상에서 IO를 처리 할 때 쓴다.
 //   이에 대해 생각해보고 이유를 알았다면 ASSERT_EQ를 TRUE 하여라

 ASSERT_TRUE(true);
}
