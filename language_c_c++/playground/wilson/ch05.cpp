#include "gtest/gtest.h"

#include <stdio.h>
#include <vector>

using namespace testing;


TEST(CH05_JOEY, Basic_01) {

  int your_answer = 0;

  int sum = 10;
  int val = 0;

  do{
    int sum = 100;
    sum += val;
    ++val;
  }while (val <= 10);

  ASSERT_EQ(your_answer, sum );
}

TEST(CH05_JOEY, Basic_02) {

  int your_answer = 0;

  unsigned aCnt = 0, eCnt = 0, iCnt = 0;
  char ch = 'a';
  switch (ch) {
    case 'a':
      ++aCnt;
    case 'e':
      ++eCnt;
    case 'i':
      ++iCnt;
  }

  ASSERT_EQ(your_answer, eCnt );
}
