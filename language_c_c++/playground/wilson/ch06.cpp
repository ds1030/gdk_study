#include "gtest/gtest.h"

#include <stdio.h>
#include <cctype>

using namespace testing;

int factorial (int x) {
  int y = 1;
  while (x != 0) {
    y*=x;
    x--;
  }
  return y;
}

TEST(Function, Factorial) {
  const int input = 5;
  const int output = 1 * 2 * 3 * 4 * 5;

  // factorial 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = factorial(input);

  ASSERT_EQ(myAnswer, output);
}

int absolute(int x) {
  if (x < 0)
    x*= (-1);
  return x;
}

TEST(Function, Absolute_Negative_Input) {
  const int input = -5;
  const int output = 5;

  // absolute 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = absolute(input);

  ASSERT_EQ(myAnswer, output);
}

TEST(Function, Absolute_Zero_Input) {
  const int input = 0;
  const int output = 0;

  // absolute 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = absolute(input);

  ASSERT_EQ(myAnswer, output);
}

TEST(Function, Absolute_Positive_Input) {
  const int input = 120;
  const int output = 120;

  // absolute 함수를 정의하고 구현하라.
  int myAnswer = 0;
  myAnswer = absolute(input);

  ASSERT_EQ(myAnswer, output);
}

int countCalls() {
  static int count = 0;
  count++;
  return count;
}

TEST(Function, CountCalls) {
  const int ouput_when_first_call = 1;
  const int ouput_when_second_call = 2;
  const int ouput_when_third_call = 3;

  // countCalls 함수를 구현하라.

  EXPECT_EQ(countCalls(), ouput_when_first_call);
  EXPECT_EQ(countCalls(), ouput_when_second_call);
  EXPECT_EQ(countCalls(), ouput_when_second_call);
}

void switchValue(int &a, int &b) {
  int tmp = a;
  a = b;
  b = tmp;
}

TEST(Function, Switch) {
  int a = 25;
  int b = 100;

  // 두 변수의 값을 바꾸는 switch 함수를 구현하라.
  switchValue(a, b);

  EXPECT_EQ(a, 100);
  EXPECT_EQ(b, 25);
}

std::string toLowercase(std::string x) {
  std::string result = "";
  for (auto c : x) {
    result += tolower(c);
  }
  return result;
}

TEST(Function, ToLowercase) {
  std::string input = "ApPle";

  // 문자열 내용을 모두 소문자로 바꾸는 함수를 만든다.
  std::string myAnswer;
  myAnswer = toLowercase(input);

  EXPECT_EQ(myAnswer, "apple");
}
