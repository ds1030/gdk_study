#!/bin/bash

function findFilesAndSave
{
  echo "TARGET PATH="$1
  find $1 -type f -name "*.cpp" -o -name "*.h" -o -name "*.c" >> format-source.txt
}

function findTargetWithoutDir
{
  echo "FIND PATH="$1
  echo "WITHOUT PATH="$2
  echo "find target name="$3
  find $1 ! -path $2 -type f -name $3 >> format-source.txt
}

rm format-source.txt

findFilesAndSave ./brain
findFilesAndSave ./jayden
findFilesAndSave ./joey
findFilesAndSave ./seo
findFilesAndSave ./tom
findFilesAndSave ./wilson

cat format-source.txt | xargs clang-format -style=file -i fallback-style=none

rm format-source.txt
