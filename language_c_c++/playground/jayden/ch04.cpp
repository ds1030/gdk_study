#include "gtest/gtest.h"

#include <stdio.h>
#include <vector>

using namespace testing;


TEST(CH04_SEO, Basic_01) {

  //Q. 4.1, 5 + 10 * 20 / 2 =
  int your_answer = 105;

  ASSERT_EQ(your_answer, (5 + 10 * 20 / 2) );
}

TEST(CH04_SEO, Basic_02) {

  //Q.결합 순서를 맞추세요
  std::vector<int> vec = {123,456,789};
  int your_answer = 124;

  ASSERT_EQ(your_answer, * vec.begin() + 1 );
}

TEST(CH04_SEO, Basic_03) {

 //Q. 이항 연산자 대부분의 평가 순서는 미정의로 남겨두고 컴파일러가 최적화를 하도록 한다.
 //   이에 대해 생각해보고 이유를 알았다면 ASSERT_EQ를 TRUE 하여라

 ASSERT_TRUE(true);
}

TEST(CH04_SEO, Basic_04) {

 //Q. && || 연산자의 Operand 는 언제 평가하는지 설명해라.
 //   이에 대해 생각해보고 이유를 알았다면 ASSERT_EQ를 TRUE 하여라

 ASSERT_TRUE(true);
}

TEST(CH04_SEO, Basic_05) {
  double d;
  int i;

  //Q. d(double) = i(int) = 3.5
  d = i = 3.5;
  ASSERT_EQ(3, d);
  ASSERT_EQ(3, i);

  //Q. i(int) = d(double) = 3.5
  i = d = 3.5;
  ASSERT_EQ(3.5, d);
  ASSERT_EQ(3, i);
}
