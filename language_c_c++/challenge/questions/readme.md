

##How to build
1. Install Google Benchmark (https://github.com/google/benchmark)

```
$ git clone https://github.com/google/benchmark.git
# Benchmark requires Google Test as a dependency. Add the source tree as a subdirectory.
$ git clone https://github.com/google/googletest.git benchmark/googletest
$ mkdir build && cd build
$ cmake ../benchmark
$ make install
```

2. Build

```
$mkdir build
$cd extra_large_factorial
$cmake ../build
```
