#include <benchmark/benchmark.h>
#include <iostream>

using namespace std;

static void testcase1() {
  for(int i=0 ; i<1000000 ; i++)
    printf("");
}

static void BM_Question(benchmark::State& state) {
  //Prepare

  for (auto _ : state) {
    testcase1();
  }
}
BENCHMARK(BM_Question);
BENCHMARK(BM_Question)->ThreadPerCpu();

static_assert(std::is_same<
  typename std::iterator_traits<benchmark::State::StateIterator>::value_type,
  typename benchmark::State::StateIterator::value_type>::value, "");

BENCHMARK_MAIN();
