# C++ Study
## Basic: C++ Primer
### Rules
- 모든 사람이 당일 분량까지 챕터를 읽는다.
- 챕터의 담당자를 정한다.
- 담당자는 당일에 해당 챕터을 리뷰한다.
- 스터디 시간은 아침 8시부터 10시까지.
- 1시간은 리뷰, 1시간은 연습시간으로 한다.
- 두 번째 스터디부터는 이전 챕터들을 누적해서 리뷰한다. 담당자는 바뀐다.
- 수요일은 자율학습.
- 특별한 사유없이 늦거나 결석할 경우 10,000원
- '특별한 사유'란,
  - 결석시 새벽 6시 전에 통보
  - 연차는 제외

## Cracking the Coding Interview
### Rules
- 매일 1시간은 자율학습
- 나머지 1시간은 문제풀이 공유 (본인 답 및 답안지)
- 컨플런스에 본인 답을 올린다.

### Attendance
|              |seo   |brian   |joey   |tom   |wilson   |jayden   |ted   |
|--------------|------|--------|-------|------|---------|---------|------|
|2018.06.22.Fri|O     |O       |O      |O     |O        |-        |-     |
|2018.06.25.Mon|O     |O       |O      |O     |O        |O        |O     |
|2018.06.26.Tue|O     |-       |O      |O     |O        |O        |O     |
|2018.06.27.Wed|O     |-       |-      |O     |O        |O        |O     |
|2018.06.28.Thu|O     |-       |O      |O     |O        |O        |O     |
|2018.06.29.Fri|O     |-       |X      |O     |O        |O        |O     |
|2018.07.02.Mon|O     |O       |^      |X     |O        |O        |O     |
|2018.07.03.Tue|O     |-       |O      |O     |O        |O        |O     |
|2018.07.04.Wed|O     |O       |O      |O     |O        |O        |O     |
|2018.07.05.Thu|-     |O       |^      |O     |O        |O        |O     |
|2018.07.06.Fri|-     |O       |O      |O     |O        |O        |-     |
|2018.07.09.Mon|O     |O       |O      |O     |O        |O        |O     |
|2018.07.10.Tue|-     |O       |O      |X     |O        |O        |-     |
|2018.07.11.Wed|-     |O       |O      |-     |O        |O        |-     |
|2018.07.12.Thu|-     |O       |O      |O     |O        |O        |-     |
|2018.07.13.Fri|-     |O       |-      |-     |O        |-        |-     |
|2018.07.16.Mon|-     |O       |O      |O     |O        |-        |-     |
|2018.07.17.Tue|-     |O       |O      |O     |O        |-        |-     |
|2018.07.18.Wed|-     |O       |O      |O     |O        |^        |-     |
|2018.07.19.Thu|-     |O       |^      |O     |O        |O        |-     |
|2018.07.23.Mon|O     |O       |O      |O     |-        |O        |O     |
|2018.07.24.Tue|-     |O       |O      |O     |O        |^        |-     |

- Joey 30,000 won (지급 완료)
- Tom 20,000 won (지급 완료)
- Jayden 30,000 won
- Joey 10,000 won

### Log
#### 2018.07.25.Wed

#### 2018.07.24.Tue
- chapter 02 Linked List
  - 완료
    - 1, 2, 3, 4
  - 다음 과제
    - 5, 6, 7

#### 2018.07.20.Fri
#### 2018.07.19.Thu
- 알고리즘 풀기

#### 2018.07.18.Wed
- 알고리즘 풀기

#### 2018.07.17.Tue
- 알고리즘 풀기

#### 2018.07.16.Mon
- 연습문제 완료한 챕터
  ch 01, 02, 03, 04

- 다음 스터디 챕터 담당자 결정
  - Chapter 06 Functions                  Jayden
  - Chapter 07 Class,                     Joey
  - Chapter 09 Sequence Container         Ted
  - Chapter 12 Dynamic Memory             Brian

#### 2018.07.13.Fri
- 자율학습

#### 2018.07.12.Thu
- 다음부터는
  - 1h 하드스탑.
- 가려운 부분
  - Algorithm
  - OS
    - UNIX
  - Shared Memory
  - MVVM vs MVC
  - Docker
- 다음 스터디 챕터 담당자 결정
  - Chapter 02 연습문제                      Brian
  - Chapter 03 연습문제                      Joey
  - Algorithm:Craking the coding Interview, Chapter 01 Tom
  - Chapter

#### 2018.07.11.Wed
- 다음 스터디 챕터 담당자 결정
  - if문, reinterpret_cast, mutex         Tom
  - Chapter 연습문제 2.4-3.3                Wilson
  - Chapter 07 Class                      Brian
  - Chapter 연습문제 3.4-3.6                Joey
  - Chapter 13 뒷부분                      Jayden

#### 2018.07.09.Mon
- 다음 스터디 챕터 담당자 결정
  - if문, reinterpret_cast, mutex          Tom
  - Chapter 02 변수                        Wilson
  - Chapter 07 Class                      Brian
  - Chapter 12 Dynamic Memory             Joey
  - Chapter 13 복사 제어와 자원관리            Jayden

#### 2018.07.06.Fri
- 다음 스터디 챕터 담당자 결정
  - Chapter 07 Class                      Brian
  - Chapter 10 Algorithm 중 람다            Ted
  - Chapter 12 Dynamic Memory             Joey
  - Chapter 13 복사 제어와 자원관리            Jayden
  - Chapter 14 다중 정의 연산과 변환           Seo
  - RTTI, volatile, macro, 예외처리          Wilson
  - if문, reinterpret_cast, mutex          Tom

#### 2018.07.05.Thu
- 완료한 챕터
  - Chapter 15 객체 지향 프로그래밍            Tom
  - Chapter 16 템플릿과 일반화 프로그래밍        Wilson

- 다음 스터디 챕터 담당자 결정
  - Chapter 14 다중 정의 연산과 변환           Seo
  - Chapter 17 정규표현식                    Ted
  - Chapter 18 네임스페이스                  Joey + Seo
  - Chapter 19 실행 중 타입 식별              Jayden + Seo

#### 2018.07.04.Wed
- 자율학습

#### 2018.07.03.Tue
- 완료한 챕터
  - Chapter 10 Algorithm                  Jayden
  - Chapter 13 복사 제어와 자원관리            Joey

- 다음 스터디 챕터 담당자 결정
  - Chapter 14 다중 정의 연산과 변환           Seo
  - Chapter 15 객체 지향 프로그래밍            Tom
  - Chapter 16 템플릿과 일반화 프로그래밍        Wilson
  - Chapter 17 정규표현식                    Ted
  - Chapter 18 네임스페이스                  Joey + Seo
  - Chapter 19 실행 중 타입 식별              Jayden + Seo

#### 2018.07.02.Mon
- 완료한 챕터
  - Chapter 12 Dynamic Memory             Ted

- 다음 스터디 챕터 담당자 결정
  - Chapter 10 Algorithm                  Jayden
  - Chapter 13 복사 제어와 자원관리            Joey
  - Chapter 14 다중 정의 연산과 변환           Seo
  - Chapter 15 객체 지향 프로그래밍            Tom
  - Chapter 16 템플릿과 일반화 프로그래밍        Wilson
  - Chapter 17 정규표현식                    Ted

#### 2018.06.29.Fri
- 자습

#### 2018.06.28.Thu
- 완료한 챕터
  - Chapter 02 Variable                   Jayden
  - Chapter 05 Statements,                Brian
  - Chapter 07 Class,                     Joey
  - Chapter 09 Sequence Container         Ted
  - Chapter 11 Associated Container       Seo

- 다음 스터디 챕터 담당자 결정
  - Chapter 10 Algorithm                  Jayden
  - Chapter 12 Dynamic Memory             Ted
  - Chapter 13 복사 제어와 자원관리            Joey
  - Chapter 14 다중 정의 연산과 변환           Seo
  - Chapter 15 객체 지향 프로그래밍            Tom
  - Chapter 16 템플릿과 일반화 프로그래밍        Wilson

#### 2018.06.26.Tue
- 완료한 챕터
  - Chapter 01 Getting Started,
  - Chapter 03 Character, Vector, Array,
  - Chapter 04 Expression,
  - Chapter 06 Functions,
  - Chapter 08 Class,

- 다음 스터디 챕터 담당자 결정
  - Chapter 02 Variable                   Jayden
  - Chapter 05 Statements,                Brian
  - Chapter 07 Class,                     Joey
  - Chapter 09 Sequence Container         Ted
  - Chapter 10 Algorithm                  Wilson
  - Chapter 11 Associated Container       Seo
  - Chapter 12 Dynamic Memory             Tom

#### 2018.06.25.Mon
- 다음 스터디 챕터 담당자 결정
  - Chapter 01 Getting Started,           Seo
  - Chapter 02 Variable                   Jayden
  - Chapter 03 Character, Vector, Array,  Wilson
  - Chapter 04 Expression,                Ted
  - Chapter 05 Statements,                Brian
  - Chapter 06 Functions,                 Tom
  - Chapter 07 Class,                     Joey

#### 2018.06.22.Fri
- 스터디 진행방식 결정
- Seo가 Git 연습문제 프로젝트 공유
- 다음 스터디 챕터 담당자 결정
  - Chapter 01 Getting Started,           Brian
  - Chapter 02 Variable                   Jayden
  - Chapter 03 Character, Vector, Array,  Ted
  - Chapter 04 Expression,                Seo
  - Chapter 05 Statements,                Joey
  - Chapter 06 Functions,                 Wilson
  - Chapter 07 Class,                     Tom
